###ver. 12.07.2016
###Program rysuje profile energii swobodnej, entropii, entaplii oraz zliczenia na podstawie maciezy rdf'ow dla różnych temparatur.

import math
import sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

figure = plt.figure()								###ustawienia rozmiarow rysunku
plt.subplot(2,2,1)
figure.set_dpi(200)
figure.set_size_inches(12, 8)

mpl.rcParams['axes.linewidth'] = 2						###opcje czcionek
mpl.rcParams.update({'font.size': 16})
plt.rc('grid', color='0', linestyle='-', linewidth=0.5)

image = np.loadtxt('tmp_all')							###ladowanie plikow
temp = np.loadtxt('temp.dat')

ss=len(image[0])								###sprawdzenie rozmiaru plikow
if len(image)!=50:
	print "Zla liczba wierszy!!!"
	sys.exit(0)

count = np.zeros(shape=(50,ss))							###dodatkowa kopia rdfow-zliczen
for i in range(0,50):
	for j in range (0,ss):
		count[i][j]=image[i][j]
prze=700
for i in range (0,50):								###obliczanie energii swobodnej
	for j in range (0,ss):					
		if image[i][j] == 0:
			image[i][j] = 1						###zamiana 0 na 1, bo log zle dziala
		image[i][j]=-0.00198815*temp[i]*math.log(image[i][j])
	dod=(image[i][prze-3]+image[i][prze-2]+image[i][prze-1]+image[i][prze]+image[i][prze+1]+image[i][prze+2]+image[i][prze+3])/7
	image[i]=image[i]-dod							###przesuwanie profili

image2 = np.zeros(shape=(50,ss))
image3 = np.zeros(shape=(50,ss))

for i in range(0,48):								###obliczanie entropii, pochodna
	for j in range (0,ss):
		image2[i+1][j] = (image[(i+2)][j]-image[i][j])/(temp[i+2]-temp[i])*temp[i+1]

image2[0][:]=image2[1][:]							###uzupelnianie skrajnych wartosci
image2[49][:]=image2[48][:]

image3=image-image2								###obliczanie entalpii

#for i in range(0,50):								###opcjonalne przesuwanie entropii i entalpii
#	dod2=(image2[i][697]+image2[i][698]+image2[i][699]+image2[i][700]+image2[i][701]+image2[i][702]+image2[i][703])/7
#	dod3=(image3[i][697]+image3[i][698]+image3[i][699]+image3[i][700]+image3[i][701]+image3[i][702]+image3[i][703])/7
#	image2[i]=image2[i]-dod2
#	image3[i]=image3[i]-dod3

sss=ss/10

ima1 = np.zeros(shape=(50,sss))					
ima2 = np.zeros(shape=(50,sss))
ima3 = np.zeros(shape=(50,sss))
cou = np.zeros(shape=(50,sss))

for i in range(0,sss):								###redukcja danych ze wzgledu na metode rysowania
	for j in range(0,50):
		ima1[j][i]=image[j][i*10]
		ima2[j][i]=image2[j][i*10]
		ima3[j][i]=image3[j][i*10]
		cou[j][i]=count[j][i*10]

cmap = LinearSegmentedColormap.from_list('mycmap', [(0.0, 'white'),		###kolory paska wartosci
						    (0.33, 'blue'),
                                                    (0.66, 'red'),
						    (1.0, 'yellow')]
                                        )

#Energia swobodna

print "energia", ima1[:,14:69].min(), ima1[:,14:69].max()			###wartosci min i max
plt.imshow(ima1, cmap=cmap, norm = mpl.colors.Normalize(vmin=0, vmax=1.5))	###wartosci min max pionowego slupka
cbar = plt.colorbar(fraction=0.028, ticks=[0,0.5,1,1.5])			###podpisy pionowego slupka
plt.grid()

plt.yticks([0,9,19,29,39,49],["300","332","370","413","459","510"])		###podpisy osi y
plt.xticks([0,9,19,29,39,49,59,69,79,89,99,109,119,129,139],["0.0","0.2","0.4","0.6","0.8","1.0","1.2","1.4","1.6","1.8","2.0","2.2","2.4"])
plt.xlim([14,69])								###zakresy osi x
plt.ylim([0,49])								###zakresy osi y

plt.ylabel("Temperature", size=20)						###podpis osi y

#Entropia

print "entropia", ima2[:,14:69].min(), ima2[:,14:69].max()			###wartosci min i max
plt.subplot(2,2,2)
plt.imshow(ima2, cmap=cmap, norm = mpl.colors.Normalize(vmin=-0.5, vmax=1))
cbar = plt.colorbar(fraction=0.028, ticks=[-0.5,0,0.5,1])
plt.grid()

plt.yticks([0,9,19,29,39,49],["300","332","370","413","459","510"])
plt.xticks([0,9,19,29,39,49,59,69,79,89,99,109,119,129,139],["0.0","0.2","0.4","0.6","0.8","1.0","1.2","1.4","1.6","1.8","2.0","2.2","2.4"])
plt.xlim([14,69])
plt.ylim([0,49])

##Entalpia

print "entalpia", ima3[:,14:69].min(), ima3[:,14:69].max()			###wartosci min i max
plt.subplot(2,2,3)
plt.imshow(ima3, cmap=cmap, norm = mpl.colors.Normalize(vmin=-0.5, vmax=1))
cbar = plt.colorbar(fraction=0.028, ticks=[-0.5,0,0.5,1])
plt.grid()

plt.yticks([0,9,19,29,39,49],["300","332","370","413","459","510"])
plt.xticks([0,9,19,29,39,49,59,69,79,89,99,109,119,129,139],["0.0","0.2","0.4","0.6","0.8","1.0","1.2","1.4","1.6","1.8","2.0","2.2","2.4"])
plt.xlim([14,69])
plt.ylim([0,49])

plt.xlabel("Distance [nm]", size=20)
plt.ylabel("Temperature", size=20)

##Zliczenia

plt.subplot(2,2,4)
plt.imshow(cou, cmap=cmap, norm = mpl.colors.Normalize(vmin=0, vmax=1400))
cbar = plt.colorbar(fraction=0.028, ticks=[0, 200, 400, 600, 800, 1000, 1200, 1400])
plt.grid()

plt.yticks([0,9,19,29,39,49],["300","332","370","413","459","510"])
plt.xticks([0,9,19,29,39,49,59,69,79,89,99,109,119,129,139],["0.0","0.2","0.4","0.6","0.8","1.0","1.2","1.4","1.6","1.8","2.0","2.2","2.4"])
plt.xlim([14,69])
plt.ylim([0,49])

plt.xlabel("Distance [nm]", size=20)
plt.savefig("C3.svg")